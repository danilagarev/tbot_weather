import datetime
import locale

from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters,
                          ConversationHandler, CallbackQueryHandler)

from config import TOKEN
from utils import get_weather_for_date

locale.setlocale(locale.LC_TIME, ('ru_RU', 'UTF-8'))

WEATHER, CANCEL, CHOOSING = range(3)


def start(update, context):
    keyboard = [
        [InlineKeyboardButton("Узнать погоду", callback_data=str(WEATHER)),
         InlineKeyboardButton("Закончить диалог", callback_data=str(CANCEL))]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text(
        f'Привет {update.message.from_user.first_name}\n'
        f'Я бот у которого ты можешь узнать погоду в Ярославле\n'
        f'Выбери что бы ты хотел сделать',
        reply_markup=reply_markup)

    return CHOOSING


def start_over(update, context):
    keyboard = [
        [InlineKeyboardButton("Узнать погоду", callback_data=str(WEATHER)),
         InlineKeyboardButton("Закончить диалог", callback_data=str(CANCEL))]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text(
        f'Выбери что бы ты хотел сделать',
        reply_markup=reply_markup)

    return CHOOSING


def weather(update, context):
    query = update.callback_query
    query.answer()
    query.edit_message_text(
        text="Введи дату в формате(1 июля) и я сообщу тебе что мне известно",
    )

    return WEATHER


def get_weather(update, context):
    date = update.message.text
    weather_data = get_weather_for_date(date)
    if weather_data:
        text = f'Вот что мне известно:\n' \
               f'\n' \
               f'{weather_data}'
    else:
        text = f"Попробуй ввести дату в формате ({datetime.datetime.today().strftime('%d %B')}) \n" \
               f"И в рамках от {datetime.datetime.today().strftime('%d %B')}" \
               f" до {(datetime.datetime.now() + datetime.timedelta(days=30)).date().strftime('%d %B')}"

    keyboard = [
        [InlineKeyboardButton("Закончить диалог", callback_data=str(CANCEL))]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text(text, reply_markup=reply_markup)

    return WEATHER


def cancel(update, context):
    try:
        query = update.callback_query
        query.answer()
        query.edit_message_text(
            text="До встречи!\n"
                 "Если я понадоблюсь отправь /start",
        )

        return ConversationHandler.END
    except AttributeError:
        update.message.reply_text("До встречи!\n"
                                  "Если я понадоблюсь отправь /start")

        return ConversationHandler.END


def main():
    updater = Updater(TOKEN, use_context=True)
    dp = updater.dispatcher

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],

        states={
            CHOOSING: [CallbackQueryHandler(weather, pattern='^' + str(WEATHER) + '$'),
                       CallbackQueryHandler(cancel, pattern='^' + str(CANCEL) + '$')],
            WEATHER: [MessageHandler(Filters.text, get_weather),
                      CallbackQueryHandler(cancel, pattern='^' + str(CANCEL) + '$')]
        },
        fallbacks=[CommandHandler('cancel', cancel)]
    )

    dp.add_handler(conv_handler)
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
